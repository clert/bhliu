﻿"object" != typeof ue || "object" != typeof ue.interface ? ("object" != typeof ue && (ue = {}), ue.interface = {}, ue.interface.broadcast = function(e, t) {
    if ("string" == typeof e) {
        var o = [e, ""];
        void 0 !== t && (o[1] = t);
        var n = encodeURIComponent(JSON.stringify(o));
        "object" == typeof history && "function" == typeof history.pushState ? (history.pushState({}, "", "#" + n), history.pushState({}, "", "#" + encodeURIComponent("[]"))) : (document.location.hash = n, document.location.hash = encodeURIComponent("[]"))
    }
}) : function(e) { ue.interface = {}, ue.interface.broadcast = function(t, o) { "string" == typeof t && (void 0 !== o ? e.broadcast(t, JSON.stringify(o)) : e.broadcast(t, "")) } }(ue.interface), (ue4 = ue.interface.broadcast);





$(function() {
    echarts_1();
    echarts_2();
    echarts_3();
    echarts_4();

    function echarts_1() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echart1'));
        myChart.on("click", params => {

            ue4("Print", params.dataIndex)

        });

        option = {
            //  backgroundColor: '#00265f',
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                }
            },
            grid: {
                left: '0%',
                top: '10px',
                right: '0%',
                bottom: '4%',
                containLabel: true
            },
            xAxis: [{
                type: 'category',
                data: ['02-20', '02-21', '02-22', '02-23', '02-24', '02-25', '02-26'],
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: "rgba(255,255,255,.1)",
                        width: 1,
                        type: "solid"
                    },
                },

                axisTick: {
                    show: false,
                },
                axisLabel: {
                    interval: 0,
                    // rotate:50,
                    show: true,
                    splitNumber: 15,
                    textStyle: {
                        color: "rgba(255,255,255,.6)",
                        fontSize: '12',
                    },
                },
            }],
            yAxis: [{
                type: 'value',
                axisLabel: {
                    //formatter: '{value} %'
                    show: true,
                    textStyle: {
                        color: "rgba(255,255,255,.6)",
                        fontSize: '12',
                    },
                },
                axisTick: {
                    show: false,
                },
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: "rgba(255,255,255,.1	)",
                        width: 1,
                        type: "solid"
                    },
                },
                splitLine: {
                    lineStyle: {
                        color: "rgba(255,255,255,.1)",
                    }
                }
            }],
            series: [{
                    name: '降雨量（mm）',
                    type: 'bar',
                    data: [20, 30, 30, 90, 170, 130, 40],
                    barWidth: '35%', //柱子宽度
                    // barGap: 1, //柱子之间间距
                    itemStyle: {
                        normal: {
                            color: '#2f89cf',
                            opacity: 1,
                            barBorderRadius: 5,
                        }
                    }
                }

            ]
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize", function() {
            myChart.resize();
        });
    }

    function echarts_2() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echart2'));

        
        option = {
            //  backgroundColor: '#00265f',
            tooltip: {
                trigger: 'axis',
                axisPointer: { type: 'shadow' }
            },
            grid: {
                left: '0%',
                top: '10px',
                right: '0%',
                bottom: '4%',
                containLabel: true
            },
            xAxis: {
                type: 'category',
                data: ['12-06', '12-07', '12-08', '12-09', '12-10', '12-11', '12-12'],
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: "rgba(255,255,255,.1)",
                        width: 1,
                        type: "solid"
                    },
                },

                axisTick: {
                    show: false,
                },
                axisLabel: {
                    interval: 0,
                    // rotate:50,
                    show: true,
                    splitNumber: 15,
                    textStyle: {
                        color: "rgba(255,255,255,.6)",
                        fontSize: '12',
                    },
                }
            },
            yAxis: {
                type: 'value',
                axisLabel: {
                    //formatter: '{value} %'
                    show: true,
                    textStyle: {
                        color: "rgba(255,255,255,.6)",
                        fontSize: '12',
                    },
                }
            },
            series: [{
                data: [1200, 2000, 1500, 800, 700, 1100, 1300],
                type: 'line',
                symbol: 'triangle',
                symbolSize: 20,
                lineStyle: {
                    color: 'green',
                    width: 4,
                    type: 'dashed'
                },
                name: 'm²/h',
                itemStyle: {
                    borderWidth: 3,
                    borderColor: 'yellow',
                    color: 'blue'
                }
            }],
        };

        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize", function() {
            myChart.resize();
        });
    }

    function echarts_3() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echart3'));
        
        option = {
            color: ["#EAEA26", "#906BF9", "#FE5656", "#01E17E", "#3DD1F9", "#FFAD05"],
            grid: {
                left: -100,
                top: 50,
                bottom: 10,
                right: 10,
                containLabel: true
            },
            tooltip: {
                trigger: 'item',
                formatter: "{b} : {c} ({d}%)"
            },
            legend: {
                type: "scroll",
                orient: "vartical",
                // x: "right",
                top: "center",
                right: "15",
                // bottom: "0%",
                itemWidth: 16,
                itemHeight: 8,
                itemGap: 16,
                textStyle: {
                    color: '#A3E2F4',
                    fontSize: 12,
                    fontWeight: 0
                },
                data:  ['TP', 'TN', 'COD', 'NH₃-N']
            },
            polar: {},
            angleAxis: {
                interval: 1,
                type: 'category',
                data: [],
                z: 10,
                axisLine: {
                    show: false,
                    lineStyle: {
                        color: "#183ED1",
                        width: 1,
                        type: "solid"
                    },
                },
                axisLabel: {
                    interval: 0,
                    show: true,
                    color: "#183ED1",
                    margin: 8,
                    fontSize: 16
                },
            },
            radiusAxis: {
                min: 40,
                max: 120,
                interval: 20,
                axisLine: {
                    show: false,
                    lineStyle: {
                        color: "#183ED1",
                        width: 1,
                        type: "solid"
                    },
                },
                axisLabel: {
                    formatter: '{value} %',
                    show: false,
                    padding: [0, 0, 20, 0],
                    color: "#183ED1",
                    fontSize: 16
                },
                splitLine: {
                    lineStyle: {
                        color: "#183ED1",
                        width: 2,
                        type: "solid"
                    }
                }
            },
            calculable: true,
            series: [{
                type: 'pie',
                radius: ["5%", "10%"],
                hoverAnimation: false,
                labelLine: {
                    normal: {
                        show: false,
                        length: 30,
                        length2: 55
                    },
                    emphasis: {
                        show: false
                    }
                },
                data: [{
                    name: '',
                    value: 0,
                    itemStyle: {
                        normal: {
                            color: "#183ED1"
                        }
                    }
                }]
            }, {
                type: 'pie',
                radius: ["90%", "95%"],
                hoverAnimation: false,
                labelLine: {
                    normal: {
                        show: false,
                        length: 30,
                        length2: 55
                    },
                    emphasis: {
                        show: false
                    }
                },
                name: "",
                data: [{
                    name: '',
                    value: 0,
                    itemStyle: {
                        normal: {
                            color: "#183ED1"
                        }
                    }
                }]
            },{
                stack: 'a',
                type: 'pie',
                radius: ['20%', '80%'],
                roseType: 'area',
                zlevel:10,
                label: {
                    normal: {
                        show: true,
                        formatter: "{c}",
                        textStyle: {
                            fontSize: 12,
                        },
                        position: 'outside'
                    },
                    emphasis: {
                        show: true
                    }
                },
                labelLine: {
                    normal: {
                        show: true,
                        length: 20,
                        length2: 55
                    },
                    emphasis: {
                        show: false
                    }
                },
                data: [
                    {
                        value: 15,
                        name: 'TP'
                    },
                    {
                        value: 25,
                        name: 'TN'
                    },
                    {
                        value: 20,
                        name: 'COD'
                    },
                    {
                        value: 35,
                        name: 'NH₃-N'
                    }
                ]
            }, ]
        }
       
        
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize", function() {
            myChart.resize();
        });
    }

    function echarts_4() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echart4'));
            
        //颜色16进制换算rgba,添加透明度
        function hexToRgba(hex, opacity) {
            return (
                'rgba(' +
                parseInt('0x' + hex.slice(1, 3)) +
                ',' +
                parseInt('0x' + hex.slice(3, 5)) +
                ',' +
                parseInt('0x' + hex.slice(5, 7)) +
                ',' +
                opacity +
                ')'
            );
        }

        // 数据
        chartdata = [
            {
                name: 'I类',
                value: 30,
            },
            {
                name: 'II类',
                value: 50,
            },
            {
                name: 'III类',
                value: 80,
            },
            {
                name: 'IV类',
                value: 90,
            },
            {
                name: 'V类',
                value: 90,
            },
        ];
        radius = ['30%', '34%'];
        // 颜色系列
        color = ['#37FFC9', '#FFE777', '#19D6FF', '#32A1FF', '#cccccc', '#ff1111'];
        labelshow = true;
        centerimg = true;
        lineshow = false;
        let color1 = [],
            color2 = [],
            color3 = [];
        // 设置每层圆环颜色和添加间隔的透明色
        color.forEach((item) => {
            color1.push(item, 'transparent');
            color2.push(hexToRgba(item, 0.7), 'transparent');
            color3.push(hexToRgba(item, 0.4), 'transparent');
        });
        let data1 = [];
        let sum = 0;
        // 根据总值设置间隔值大小
        chartdata.forEach((item) => {
            sum += Number(item.value);
        });
        // 给每个数据后添加特定的透明的数据形成间隔
        chartdata.forEach((item, index) => {
            if (item.value !== 0) {
                data1.push(item, {
                    name: '',
                    value: sum / 70,
                    labelLine: {
                        show: false,
                        lineStyle: {
                            color: 'transparent',
                        },
                    },
                    itemStyle: {
                        color: 'transparent',
                    },
                });
            }
        });
        // 每层圆环大小
        let radius2 = [Number(radius[1].split('%')[0]) + 0 + '%', Number(radius[1].split('%')[0]) + 4 + '%'];
        let radius3 = [Number(radius[1].split('%')[0]) + 4 + '%', Number(radius[1].split('%')[0]) + 8 + '%'];
        option = {
            legend: {
                textStyle: {
                    color: 'rgba(255,255,255)',
                    fontSize: '12',
                },
                orient: 'vertical',
                left: 10,
                x:'left', 
                y:'bottom',   
                data: ['I类', 'II类', 'III类', 'IV类', 'V类']
            },
            grid: {
                top: 0,
                bottom: 0,
                left: 0,
                right: 0,
                containLabel: true,
            },
            tooltip: {
                formatter: (params) => {
                    if (params.name !== '') {
                        return params.name + ' : ' + params.value + '\n' + '(' + params.percent + '%)';
                    }
                },
            },
            series: [
                // 最外层圆环
                {
                    type: 'pie',
                    radius: radius3,
                    center: ['50%', '50%'],
                    hoverAnimation: false,
                    startAngle: 90,
                    selectedMode: 'single',
                    selectedOffset: 0,
                    itemStyle: {
                        normal: {
                            color: (params) => {
                                return color1[params.dataIndex];
                            },
                        },
                    },
                    label: {
                        show: true,
                        position: 'outside',
                        formatter: (params) => {
                            let zzb = 0;
                            data1.forEach((item, index) => {
                                // 当前值一半加上前面的值是否大于50%（判断label朝向）
                                if (index <= params.dataIndex && item.name != '') {
                                    if (index == params.dataIndex) {
                                        zzb += Number(item.value) / 2;
                                    } else {
                                        zzb += Number(item.value);
                                    }
                                }
                            });
                            if (params.name != '') {
                                // 若当前值一半加上前面的值的占比大于0.5三角形朝右，相反朝左
                                if (zzb / sum > 0.5) {
                                    return '{txt|' + ((params.value * 100) / sum).toFixed(0) + '%}' + '\n{san|▶}';
                                } else {
                                    return '{txt|' + ((params.value * 100) / sum).toFixed(0) + '%}' + '\n{san|◀}';
                                }
                            }
                        },
                        align: 'left',
                        padding: [0, -50, 10, -60],
                        rich: {
                            txt: {
                                color: '#fff',
                                width: 10,
                                height: 10,
                                padding: [10, 10, 0, 24],
                            },
                            san: {
                                padding: [12, -3, -20, 10],
                            },
                        },
                    },
                    labelLine: {
                        show: true,
                        length: 15,
                        length2: 60,
                        lineStyle: {
                            color: '#fff',
                            width: 2,
                        },
                    },
                    data: data1,
                    z: 666,
                },
                {
                    type: 'pie',
                    radius: radius2,
                    center: ['50%', '50%'],
                    hoverAnimation: false,
                    startAngle: 90,
                    selectedMode: 'single',
                    selectedOffset: 0,
                    itemStyle: {
                        normal: {
                            color: (params) => {
                                return color2[params.dataIndex];
                            },
                        },
                    },
                    label: {
                        show: false,
                        formatter: '{b}' + ' ' + '{c}',
                    },
                    data: data1,
                    z: 666,
                },
                {
                    type: 'pie',
                    radius: radius,
                    center: ['50%', '50%'],
                    hoverAnimation: false,
                    startAngle: 90,
                    selectedMode: 'single',
                    selectedOffset: 0,
                    itemStyle: {
                        normal: {
                            color: (params) => {
                                return color3[params.dataIndex];
                            },
                        },
                    },
                    label: {
                        show: false,
                        formatter: '{b}' + ' ' + '{c}',
                    },
                    data: data1,
                    z: 666,
                },
            ],
        };
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize", function() {
            myChart.resize();
        });
    }
})