﻿"object" != typeof ue || "object" != typeof ue.interface ? ("object" != typeof ue && (ue = {}), ue.interface = {}, ue.interface.broadcast = function(e, t) {
    if ("string" == typeof e) {
        var o = [e, ""];
        void 0 !== t && (o[1] = t);
        var n = encodeURIComponent(JSON.stringify(o));
        "object" == typeof history && "function" == typeof history.pushState ? (history.pushState({}, "", "#" + n), history.pushState({}, "", "#" + encodeURIComponent("[]"))) : (document.location.hash = n, document.location.hash = encodeURIComponent("[]"))
    }
}) : function(e) { ue.interface = {}, ue.interface.broadcast = function(t, o) { "string" == typeof t && (void 0 !== o ? e.broadcast(t, JSON.stringify(o)) : e.broadcast(t, "")) } }(ue.interface), (ue4 = ue.interface.broadcast);





$(function() {
    echarts_1();
    echarts_2();
    echarts_4();

    function echarts_1() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echart1'));
        myChart.on("click", params => {

            ue4("Print", params.dataIndex)

        });

       
        const colorList = ["#9E87FF", '#73DDFF', '#fe9a8b', '#F56948', '#9E87FF']
        option = {
            title: {
                text: '电导率',
                textStyle: {
                    fontSize: 12,
                    fontWeight: 400,
                    color: '#FFF'
                },
                left: 'center',
                top: '5%'
            },
            legend: {
                icon: 'circle',
                top: '5%',
                right: '5%',
                itemWidth: 6,
                itemGap: 20,
                textStyle: {
                    color: '#FFF'
                }
            },
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    label: {
                        show: true,
                        backgroundColor: '#fff',
                        color: '#556677',
                        borderColor: 'rgba(0,0,0,0)',
                        shadowColor: 'rgba(0,0,0,0)',
                        shadowOffsetY: 0
                    },
                    lineStyle: {
                        width: 0
                    }
                },
                backgroundColor: '#fff',
                textStyle: {
                    color: '#5c6c7c'
                },
                padding: [10, 10],
                extraCssText: 'box-shadow: 1px 0 2px 0 rgba(163,163,163,0.5)'
            },
            grid: {
                top: '15%'
            },
            xAxis: [{
                type: 'category',
                data: ['0', '100', '200', '300', '400', '500', '600'],
                axisLine: {
                    lineStyle: {
                        color: '#FFF'
                    }
                },
                axisTick: {
                    show: false
                },
                axisLabel: {
                    interval: 0,
                    textStyle: {
                        color: '#FFF'
                    },
                    // 默认x轴字体大小
                    fontSize: 12,
                    // margin:文字到x轴的距离
                    margin: 15
                },
                axisPointer: {
                    label: {
                        // padding: [11, 5, 7],
                        padding: [0, 0, 10, 0],
                        margin: 15,
                        // 移入时的字体大小
                        fontSize: 12,
                        backgroundColor: {
                            type: 'linear',
                            x: 0,
                            y: 0,
                            x2: 0,
                            y2: 1,
                            colorStops: [{
                                offset: 0,
                                color: '#fff' // 0% 处的颜色
                            }, {
                                // offset: 0.9,
                                offset: 0.86,
                                /*
        0.86 = （文字 + 文字距下边线的距离）/（文字 + 文字距下边线的距离 + 下边线的宽度）
                                
                                */
                                color: '#fff' // 0% 处的颜色
                            }, {
                                offset: 0.86,
                                color: '#33c0cd' // 0% 处的颜色
                            }, {
                                offset: 1,
                                color: '#33c0cd' // 100% 处的颜色
                            }],
                            global: false // 缺省为 false
                        }
                    }
                },
                boundaryGap: false
            }],
            yAxis: [{
                type: 'value',
                axisTick: {
                    show: false
                },
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: '#FFF'
                    }
                },
                axisLabel: {
                    textStyle: {
                        color: '#FFF'
                    }
                },
                splitLine: {
                    show: false
                }
            }, {
                type: 'value',
                position: 'right',
                axisTick: {
                    show: false
                },
                axisLabel: {
                    textStyle: {
                        color: '#556677'
                    },
                    formatter: '{value}'
                },
                axisLine: {
                    show: true,
                    lineStyle: {
                        color: '#DCE2E8'
                    }
                },
                splitLine: {
                    show: false
                }
            }],
            series: [{
                    name: 'True',
                    type: 'line',
                    data: [10, 10, 30, 12, 15, 3, 7],
                    symbolSize: 1,
                    symbol: 'circle',
                    smooth: true,
                    yAxisIndex: 0,
                    showSymbol: false,
                    lineStyle: {
                        width: 5,
                        color: new echarts.graphic.LinearGradient(0, 1, 0, 0, [{
                                offset: 0,
                                color: '#9effff'
                            },
                            {
                                offset: 1,
                                color: '#9E87FF'
                            }
                        ]),
                        shadowColor: 'rgba(158,135,255, 0.3)',
                        shadowBlur: 10,
                        shadowOffsetY: 20
                    },
                    itemStyle: {
                        normal: {
                            color: colorList[0],
                            borderColor: colorList[0]
                        }
                    }
                }, {
                    name: 'Pred',
                    type: 'line',
                    data: [5, 12, 11, 14, 25, 16, 10],
                    symbolSize: 1,
                    symbol: 'circle',
                    smooth: true,
                    yAxisIndex: 0,
                    showSymbol: false,
                    lineStyle: {
                        width: 5,
                        color: new echarts.graphic.LinearGradient(1, 1, 0, 0, [{
                                offset: 0,
                                color: '#73DD39'
                            },
                            {
                                offset: 1,
                                color: '#73DDFF'
                            }
                        ]),
                        shadowColor: 'rgba(115,221,255, 0.3)',
                        shadowBlur: 10,
                        shadowOffsetY: 20
                    },
                    itemStyle: {
                        normal: {
                            color: colorList[1],
                            borderColor: colorList[1]
                        }
                    }
                }
            ]
        };
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize", function() {
            myChart.resize();
        });
    }

    function echarts_2() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echart2'));

        var color = [
            '#0CD2E6',
            '#3751E6',
            '#FFC722',
            '#886EFF',
            '#008DEC',
            '#114C90',
            '#00BFA5',
        ];
        
        var legend = [
            '一级预警',
            '二级预警',
            '三级预警',
            '水域预警'
        ];
        
        var seriesData = [
            { "name": "一级预警", "value": 5 },
            { "name": "二级预警", "value": 4 },
            { "name": "三级预警", "value": 2 },
            { "name": "水域预警", "value": 23 }
        ]
        
        var option = {
            color: color,
            grid: {
                top: '15%',
                left: 0,
                right: '1%',
                bottom: 5,
                containLabel: true,
            },
            legend: {
                orient: 'vertical',
                top: 'top',
                left: 0,
                textStyle: {
                    align: 'left',
                    verticalAlign: 'middle',
                    rich: {
                        name: {
                            color: 'rgba(255,255,255)',
                            fontSize: 10,
                        },
                        value: {
                            color: 'rgba(255,255,255)',
                            fontSize: 10,
                        },
                        rate: {
                            color: 'rgba(255,255,255)',
                            fontSize: 10,
                        },
                    },
                },
                data: legend,
                formatter: (name) => {
                    if (seriesData.length) {
                        const item = seriesData.filter((item) => item.name === name)[0];
                        return `{name|${name}：}{value| ${item.value}} {rate| ${item.value}%}`;
                    }
                },
            },
            series: [{
                name: '预警类型占比',
                type: 'pie',
                center: ['50%', '50%'],
                radius: ['45%', '65%'],
                label: {
                    normal: {
                        show: false,
                        position: 'center',
                        formatter: '{value|{c}}\n{label|{b}}',
                        rich: {
                            value: {
                                padding: 5,
                                align: 'center',
                                verticalAlign: 'middle',
                                fontSize: 32,
                            },
                            label: {
                                align: 'center',
                                verticalAlign: 'middle',
                                fontSize: 16,
                            },
                        },
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '12',
                        },
                    },
                },
                labelLine: {
                    show: false,
                    length: 0,
                    length2: 0,
                },
                data: seriesData,
            }],
        };
        
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize", function() {
            myChart.resize();
        });
    }

    function echarts_4() {
        // 基于准备好的dom，初始化echarts实例
        var myChart = echarts.init(document.getElementById('echart4'));
            
        var option = {
               tooltip: {
                 trigger: 'axis',
                 axisPointer: { // 坐标轴指示器，坐标轴触发有效
                   type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
                 }
               },
               grid: {
                 left: '2%',
                 right: '4%',
                 bottom: '14%',
                 top:'16%',
                 containLabel: true
               },
                legend: {
               data: ['水位(m)', '水位速度(m/s)', '降雨(mm)'],
               right: 10,
               top:12,
               textStyle: {
                   color: "#fff"
               },
               itemWidth: 12,
               itemHeight: 10,
               // itemGap: 35
           },
               xAxis: {
                 type: 'category',
                 data: ['2012','2013','2014','2015','2016','2017','2018','2019'],
                 axisLine: {
                   lineStyle: {
                     color: 'white'
       
                   }
                 },
                 axisLabel: {
                   // interval: 0,
                   // rotate: 40,
                   textStyle: {
                     fontFamily: 'Microsoft YaHei'
                   }
                 },
               },
       
               yAxis: {
                 type: 'value',
                 max:'1200',
                 axisLine: {
                   show: false,
                   lineStyle: {
                     color: 'white'
                   }
                 },
                 splitLine: {
                   show: true,
                   lineStyle: {
                     color: 'rgba(255,255,255,0.3)'
                   }
                 },
                 axisLabel: {}
               },
               "dataZoom": [{
                 "show": true,
                 "height": 12,
                 "xAxisIndex": [
                   0
                 ],
                 bottom:'8%',
                 "start": 10,
                 "end": 90,
                 handleIcon: 'path://M306.1,413c0,2.2-1.8,4-4,4h-59.8c-2.2,0-4-1.8-4-4V200.8c0-2.2,1.8-4,4-4h59.8c2.2,0,4,1.8,4,4V413z',
                 handleSize: '110%',
                 handleStyle:{
                   color:"#d3dee5",
       
                 },
                 textStyle:{
                   color:"#fff"},
                 borderColor:"#90979c"
               }, {
                 "type": "inside",
                 "show": true,
                 "height": 15,
                 "start": 1,
                 "end": 35
               }],
               series: [{
                 name: '水位(m)',
                 type: 'bar',
                 barWidth: '15%',
                 itemStyle: {
                   normal: {
                       color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                           offset: 0,
                           color: '#fccb05'
                       }, {
                           offset: 1,
                           color: '#f5804d'
                       }]),
                       barBorderRadius: 12,
                   },
                 },
                 data: [400, 400, 300, 300, 300, 400, 400, 400, 300]
               },
               {
                 name: '水位速度(m/s)',
                 type: 'bar',
                 barWidth: '15%',
                 itemStyle: {
                   normal: {
                       color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                           offset: 0,
                           color: '#8bd46e'
                       }, {
                           offset: 1,
                           color: '#09bcb7'
                       }]),
                       barBorderRadius: 11,
                   }
                   
                 },
                 data: [400, 500, 500, 500, 500, 400,400, 500, 500]
               },
               {
                 name: '降雨(mm)',
                 type: 'bar',
                 barWidth: '15%',
                 itemStyle: {
                   normal: {
                       color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
                           offset: 0,
                           color: '#248ff7'
                       }, {
                           offset: 1,
                           color: '#6851f1'
                       }]),
                   barBorderRadius: 11,
                   }
                 },
                 data: [400, 600, 700, 700, 1000, 400, 400, 600, 700]
               }]
             };
       
             var app = {
               currentIndex: -1,
             };
             setInterval(function () {
               var dataLen = option.series[0].data.length;
       
               // 取消之前高亮的图形
               myChart.dispatchAction({
                 type: 'downplay',
                 seriesIndex: 0,
                 dataIndex: app.currentIndex
               });
               app.currentIndex = (app.currentIndex + 1) % dataLen;
               //console.log(app.currentIndex);
               // 高亮当前图形
               myChart.dispatchAction({
                 type: 'highlight',
                 seriesIndex: 0,
                 dataIndex: app.currentIndex,
               });
               // 显示 tooltip
               myChart.dispatchAction({
                 type: 'showTip',
                 seriesIndex: 0,
                 dataIndex: app.currentIndex
               });
       
       
             }, 1000);
        // 使用刚指定的配置项和数据显示图表。
        myChart.setOption(option);
        window.addEventListener("resize", function() {
            myChart.resize();
        });
    }
})